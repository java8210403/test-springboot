package com.testspringboot.config;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.testspringboot.domain.dto.response.ResMessageDto;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Collections;
import java.util.Objects;


@Component
public class SecurityInterceptor implements HandlerInterceptor {
    @Value("${app.xContentTypeOptions}")
    private String xContentTypeOptions;
    @Value("${app.xXssProtection}")
    private String xXssProtection;
    @Value("${app.strictTransportSecurity}")
    private String strictTransportSecurity;
    @Value("${app.xFrameOptions}")
    private String xFrameOptions;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String transport = request.getHeader("Strict-Transport-Security");
        String options = request.getHeader("X-Content-Type-Options");
        String xFrame = request.getHeader("X-Frame-Options");
        String xss = request.getHeader("X-XSS-Protection");

        if (!Objects.equals(transport, strictTransportSecurity) ||
                !Objects.equals(options, xContentTypeOptions) ||
                !Objects.equals(xFrame, xFrameOptions) ||
                !Objects.equals(xss, xXssProtection)) {
            ResMessageDto message = new ResMessageDto("F", "You dont have permission to access this API", Collections.emptyList());
            response.setContentType("application/json");
            response.getWriter().write(convertObjectToJson(message));
            return false;
        }
        return true;
    }

    public String convertObjectToJson(ResMessageDto body) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(body);
    }
}