package com.testspringboot.config;

import com.testspringboot.domain.entity.UserEntity;
import com.testspringboot.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        Optional<UserEntity> userOpt = userRepository.findByUserName(userName);

        if (!userOpt.isPresent()) {
            throw new UsernameNotFoundException("User not found with username: " + userName);
        }

        return UserDetailsImpl.build(userOpt.get());
    }

}
