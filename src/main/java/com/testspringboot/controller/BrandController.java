package com.testspringboot.controller;

import com.testspringboot.domain.dto.request.ReqBaseBrandDto;
import com.testspringboot.domain.dto.response.ResMessageDto;
import com.testspringboot.service.BrandService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/rest/v1/brand")
public class BrandController {
    @Autowired
    BrandService brandService;

    @PostMapping("/getlist")
    ResponseEntity<?> getListBrand(@Valid @RequestBody ReqBaseBrandDto requestBody){
        ResMessageDto message = brandService.getList(requestBody.getGetListFilterUnitBBrand());

        return new ResponseEntity<>(message, HttpStatus.OK);
    }
}
