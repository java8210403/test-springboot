package com.testspringboot.controller;

import com.testspringboot.config.UserDetailsImpl;
import com.testspringboot.domain.dto.request.ReqLoginDto;
import com.testspringboot.domain.dto.response.ResLoginDto;
import com.testspringboot.domain.dto.response.ResMessageDto;
import com.testspringboot.exception.AuthorizationException;
import com.testspringboot.security.JwtUtils;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.Arrays;

@RestController
@RequestMapping("/rest/v1/auth")
public class UserController {
    @Autowired
    private AuthenticationManager authenticationManager;


    @Autowired
    private JwtUtils jwtUtils;

    @PostMapping("/login")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody ReqLoginDto loginRequest, HttpServletRequest request)
            {



        try {
            Authentication authentication = null;
            UserDetailsImpl userDetails = null;
            if (loginRequest.getPassword() != null && !loginRequest.getPassword().isBlank()) {
                authentication = authenticationManager.authenticate(
                        new UsernamePasswordAuthenticationToken(loginRequest.getUsername(),
                                loginRequest.getPassword()));
                userDetails = (UserDetailsImpl) authentication.getPrincipal();
            }

            SecurityContextHolder.getContext().setAuthentication(authentication);
            String jwt = jwtUtils.generateJwtToken(authentication);

            ResLoginDto userDetail = new ResLoginDto(
                    userDetails.getUsername(),
                    jwt
            );

            ResMessageDto resLoginmessage = new ResMessageDto("T", "Success", Arrays.asList(userDetail));
            return new ResponseEntity<>(resLoginmessage, HttpStatus.OK);
        } catch (Exception e) {
            throw new AuthorizationException(e.getMessage());
        }

    }
}
