package com.testspringboot.domain.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ReqBaseBrandDto {
    private ReqSearchBrandDto getListFilterUnitBBrand;
}
