package com.testspringboot.domain.dto.request;

import lombok.Data;

@Data
public class ReqLoginDto {
    private String username;
    private String password;
}
