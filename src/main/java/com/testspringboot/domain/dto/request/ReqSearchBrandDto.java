package com.testspringboot.domain.dto.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.Data;

@Data
public class ReqSearchBrandDto {
    @Size(max = 10, message = "Invalid Input")
    @JsonProperty("P_SEARCH")
    private String pSearch;
}
