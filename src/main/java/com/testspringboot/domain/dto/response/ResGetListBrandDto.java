package com.testspringboot.domain.dto.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class ResGetListBrandDto {
    @JsonProperty("CD_BRAND")
    private String cdBrand;

    @JsonProperty("DESC_BRAND")
    private String descBrand;
}
