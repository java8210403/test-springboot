package com.testspringboot.domain.dto.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@Data
public class ResLoginDto {
    private String userName;
    private String token;
}
