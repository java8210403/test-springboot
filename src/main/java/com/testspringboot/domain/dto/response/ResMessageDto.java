package com.testspringboot.domain.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.util.List;

@Data
public class ResMessageDto {

    @JsonProperty("OUT_STAT")
    private String outStat;

    @JsonProperty("OUT_MESS")
    private String outMess;

    @JsonProperty("OUT_DATA")
    private List<?> outData;

    public ResMessageDto( String outStat, String outMess, List<?> outData) {
        this.outStat = outStat;
        this.outMess = outMess;
        this.outData = outData;
    }
}
