package com.testspringboot.domain.entity;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "tbl_brand")
public class BrandEntity {
    @Id
    @Column(name = "cd_brand", length = 5)
    private String cdBrand;


    @Column(name = "desc_brand",length = 10)
    private String descBrand;
}
