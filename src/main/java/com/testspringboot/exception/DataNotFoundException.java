package com.testspringboot.exception;

public class DataNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 6829813883332232907L;
    public DataNotFoundException(String message) {
        super(message);
    }
}
