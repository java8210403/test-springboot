package com.testspringboot.exception;

import com.testspringboot.domain.dto.response.ResMessageDto;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.multipart.MaxUploadSizeExceededException;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class RestExceptionHandler {

    @ExceptionHandler({
            DataNotFoundException.class,AuthorizationException.class,})
    public ResponseEntity<ResMessageDto> userNotFoundException(RuntimeException exception) {
        ResMessageDto message = new ResMessageDto("F", exception.getMessage(), Collections.emptyList());
        return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
    }


    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ResMessageDto> handleArgumentNotValidException(MethodArgumentNotValidException exception) {
        ResMessageDto message = new ResMessageDto("F", exception.getMessage(), Collections.emptyList());
        return new ResponseEntity<>(message, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}