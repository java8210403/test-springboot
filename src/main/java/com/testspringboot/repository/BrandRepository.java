package com.testspringboot.repository;

import com.testspringboot.domain.entity.BrandEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;
import java.util.UUID;

public interface BrandRepository extends JpaRepository<BrandEntity, String> {
    @Query(value = "SELECT BE "
                    + "FROM BrandEntity BE "
                    + "WHERE LOWER(BE.descBrand) LIKE LOWER(CONCAT('%',:search,'%')) "
                    + "ORDER BY BE.cdBrand"
    )
    List<BrandEntity> searchBrand(@Param("search") String search);
}
