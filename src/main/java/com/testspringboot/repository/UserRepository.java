package com.testspringboot.repository;

import com.testspringboot.domain.entity.BrandEntity;
import com.testspringboot.domain.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface UserRepository extends JpaRepository<UserEntity, String> {

    Optional<UserEntity> findByUserName(String userName);
}
