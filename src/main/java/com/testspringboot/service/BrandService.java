package com.testspringboot.service;

import com.testspringboot.domain.dto.request.ReqSearchBrandDto;
import com.testspringboot.domain.dto.response.ResMessageDto;

public interface BrandService {
    ResMessageDto getList(ReqSearchBrandDto request);
}
