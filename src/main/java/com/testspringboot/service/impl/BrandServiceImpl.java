package com.testspringboot.service.impl;

import com.testspringboot.domain.dto.request.ReqSearchBrandDto;
import com.testspringboot.domain.dto.response.ResGetListBrandDto;
import com.testspringboot.domain.dto.response.ResMessageDto;
import com.testspringboot.domain.entity.BrandEntity;
import com.testspringboot.exception.DataNotFoundException;
import com.testspringboot.repository.BrandRepository;
import com.testspringboot.service.BrandService;
import org.apache.coyote.BadRequestException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Collectors;

@Service
public class BrandServiceImpl implements BrandService {

    @Autowired
    BrandRepository brandRepository;

    @Override
    public ResMessageDto getList(ReqSearchBrandDto request) {
        List<BrandEntity> brandList = brandRepository.searchBrand(request.getPSearch().toLowerCase());
        if (brandList.isEmpty()){
            throw new DataNotFoundException("Invalid Input");
        }

        List<ResGetListBrandDto> result = brandList.stream().map((c)->{
            ResGetListBrandDto brand = new ResGetListBrandDto();
            brand.setCdBrand(c.getCdBrand());
            brand.setDescBrand(c.getDescBrand());
            return brand;
        }).collect(Collectors.toList());
        ResMessageDto message = new ResMessageDto("T","Success", result);
        return message;
    }
}
